import React, { useState } from 'react';
import styled from 'styled-components';
import shortid from 'shortid';
import config from './config';

const themeVersions = [...new Set(config.themes.map(theme => theme.version))];

const Container = styled.div`
  * {
    box-sizing: border-box;
  }
`;

const ToolBar = styled.div`
  display: flex;
  flex-direction: column;
`;

const UrlInput = styled.div`
  display: flex;
  align-items: center;
  margin-bottom: 10px;
  input {
    width: 500px;
    height: 20px;
    font-size: 16px;
    margin-left: 10px;
  }
  div {
    display: flex;
    align-items: center;
    justify-content: center;
    height: 20px;
    padding: 2px 10px;
    background-color: #333;
    color: #fff;
    cursor: pointer;
  }
`;

const ViewPoint = styled.div`
  display: flex;
  align-items: center;
  margin-bottom: 10px;
  label {
    display: flex;
    align-items: center;
    margin-left: 10px;
    input {
      margin-right: 5px;
    }
  }
`;

const Themes = styled.div`
  display: flex;
  align-items: center;
  flex-flow: wrap;
  margin-bottom: 10px;
`;

const Label = styled.div`
  border: solid 1px #333;
  padding: 2px 20px;
  margin: 5px;
  cursor: pointer;
  user-select: none;
  ${props => props.isSelected && `background-color: #333; color: #fff;`}
  ${props => props.isLoading && `opacity: 0.5; cursor: not-allowed;`}
`;

const IframeContainer = styled.div`
  display: flex;
  flex-direction: column;
`;

const Iframe = styled.div`
  height: ${props => (props.show ? '90vh' : '0px')};
  h2 {
    display: ${props => (props.show ? 'block' : 'none')};
  }
  iframe {
    border: ${props => (props.show ? '2px solid #333' : 'none')};
    height: 100%;
  }
`;

const Loading = styled.div`
  display: ${props => (props.show ? 'flex' : 'none')};
  align-items: center;
  justify-content: center;
  margin-top: 100px;
`;

function App() {
  const [pageUrl, setPageUrl] = useState(null);
  const [currentPageUrl, setCurrentPageUrl] = useState(null);
  const [currentViewPoint, setCurrentViewPoints] = useState('100%');
  const [currentTheme, setCurrentTheme] = useState('default');
  const [loadedThemes, setLoadedThemes] = useState([]);

  const submit = () => {
    setLoadedThemes([]);
    try {
      const url = new URL(pageUrl);
      const urlSearchParams = url.searchParams.toString();
      let currentUrl = `${pageUrl}?${shortid.generate()}`;
      if (urlSearchParams) {
        currentUrl += `&${urlSearchParams}`;
      }
      setCurrentPageUrl(currentUrl);
    } catch {
      alert('please enter a valid url');
    }
  };
  return (
    <Container>
      <ToolBar>
        <UrlInput>
          Target Url:
          <input
            onChange={e => setPageUrl(e.target.value)}
            onKeyDown={e => e.key === 'Enter' && submit()}
            type='text'
          />
          <div onClick={submit}>Submit</div>
        </UrlInput>
        View Point:
        <ViewPoint>
          {config.viewPoints.map(viewPoint => (
            <Label
              onClick={() => setCurrentViewPoints(viewPoint.value)}
              isSelected={viewPoint.value === currentViewPoint}
              key={viewPoint.label}
            >
              {viewPoint.label}
            </Label>
          ))}
        </ViewPoint>
        {themeVersions.map(version => (
          <React.Fragment key={version}>
            {`Theme ${version}:`}
            <Themes>
              {config.themes
                .filter(theme => theme.version === version)
                .map(theme => (
                  <Label
                    isSelected={['all', theme.value].includes(currentTheme)}
                    isLoading={!loadedThemes.includes(theme.value)}
                    onClick={() =>
                      loadedThemes.includes(theme.value) &&
                      setCurrentTheme(theme.value)
                    }
                    key={theme.label}
                  >
                    {theme.label}
                  </Label>
                ))}
            </Themes>
          </React.Fragment>
        ))}
      </ToolBar>
      <Loading show={currentPageUrl && !loadedThemes.includes(currentTheme)}>
        <img
          src='https://s3-ap-southeast-1.amazonaws.com/static.shoplineapp.com/web/v1/img/loaders/loader1.gif'
          alt='loading'
        />
      </Loading>
      <IframeContainer>
        {currentPageUrl &&
          config.themes.map(theme => (
            <Iframe
              show={
                ['all', theme.value].includes(currentTheme) &&
                loadedThemes.includes(currentTheme)
              }
              key={theme.label}
            >
              <h2>{theme.label}</h2>
              <iframe
                title={theme.label}
                src={`${currentPageUrl}&theme_preview=${theme.value}`}
                width={currentViewPoint || '100%'}
                onLoad={() => setLoadedThemes([...loadedThemes, theme.value])}
              ></iframe>
            </Iframe>
          ))}
      </IframeContainer>
    </Container>
  );
}

export default App;
