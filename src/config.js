const config = {
  viewPoints: [
    { label: 'Responsive', value: '100%' },
    { label: 'Desktop (1366px)', value: 1366 },
    { label: 'Tablet (768px)', value: 768 },
    { label: 'Mobile (320px)', value: 320 }
  ],
  themes: [
    { label: 'Basic', value: 'default', version: 'v1' },
    { label: 'Nightfall', value: 'dark.white', version: 'v1' },
    { label: 'Paola', value: 'paola', version: 'v1' },
    { label: 'Ell', value: 'ell', version: 'v1' },
    { label: 'Fresh Urban', value: 'freshgoods', version: 'v1' },
    { label: 'Hype', value: 'hype', version: 'v1' },
    { label: 'Studio', value: 'studio', version: 'v1' },
    { label: 'Gentleman', value: 'gentleman', version: 'v1' },
    { label: 'Lace', value: 'lace', version: 'v1' },
    { label: 'Mint', value: 'mint', version: 'v1' },
    { label: 'Lux Moss', value: 'lux.moss', version: 'v1' },
    { label: 'Simple', value: 'simple', version: 'v1' },
    { label: 'Simple Patterns', value: 'bg', version: 'v1' },
    { label: 'Boxed Patterns', value: 'boxed', version: 'v1' },
    { label: 'Rebel', value: 'rebel', version: 'v1' },
    { label: 'Swanky', value: 'swanky', version: 'v1' },
    { label: 'Slate', value: 'slate', version: 'v1' },
    { label: 'Chic', value: 'chic', version: 'v1' },
    { label: 'King', value: 'king', version: 'v1' },
    { label: 'Streetify', value: 'streetify', version: 'v1' },
    { label: 'Chapter', value: 'chapter', version: 'v2' },
    { label: 'Kingsman V2', value: 'kingsman_v2', version: 'v2' },
    { label: 'Ultra Chic', value: 'ultra_chic', version: 'v2' },
    { label: 'Sangria', value: 'sangria', version: 'v2' },
    { label: 'Bianco', value: 'bianco', version: 'v2' },
    { label: 'Doris Bien', value: 'doris_bien', version: 'v2' }
  ]
};

export default config;
